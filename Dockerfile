# Version: 0.0.1
# Base info, must start with FROM
FROM centos
#ARG
MAINTAINER ktaog6 ktaog6@126.com
LABEL version="1.0.0" location="CN"
ENV REFRESHED_AT 2019-01-02

# Run command when build
# RUN(/bin/sh -c) RUN ["", ""](no shell)
RUN ["/bin/bash"]
RUN yum -y install wget git gcc gcc-c++ make automake autoconf libtool file asciidoc xmlto c-ares-devel libev-devel gettext-devel pcre-devel
RUN yum -y update nss curl libcurl
WORKDIR /tmp 
RUN ["wget", "https://download.libsodium.org/libsodium/releases/libsodium-1.0.16.tar.gz"]
RUN ["tar", "xzvf", "libsodium-1.0.16.tar.gz"]
WORKDIR libsodium-1.0.16
RUN ./configure --prefix=/data/app/libsodium-1.0.16/ --enable-static
RUN ["make"]
RUN ["make", "install"]
WORKDIR /tmp 
RUN ["wget", "https://tls.mbed.org/download/mbedtls-2.12.0-apache.tgz"]
RUN ["tar", "xzvf", "mbedtls-2.12.0-apache.tgz"]
WORKDIR mbedtls-2.12.0
RUN ["make"]
RUN ["make", "install"]

WORKDIR /tmp 
RUN ["git", "clone", "https://github.com/shadowsocks/shadowsocks-libev.git"]
WORKDIR /tmp/shadowsocks-libev
RUN git submodule update --init --recursive
RUN ["./autogen.sh"]
RUN ./configure --prefix=/data/app/shadowsocks-libev/ --with-sodium=/data/app/libsodium-1.0.16/ --with-mbedtls=/usr/local/ --enable-static
RUN ["make"]
RUN ["make", "install"]
WORKDIR /data/app/shadowsocks-libev/
RUN ["mkdir", "etc"]
RUN ["mkdir", "logs"]
RUN echo "cd /data/app/shadowsocks-libev && nohup bin/ss-server -c etc/server.conf >> logs/shadowsocks.log 2>&1 &" >> /etc/rc.local

# add file to image, if tar.gz ,it's decompression it.
ADD server.conf /data/app/shadowsocks-libev/etc/

# Execute command when os start, can be replace by docker run
WORKDIR /root/
CMD ["/bin/bash"]

# port used
EXPOSE 10000

#STOPSIGNAL
